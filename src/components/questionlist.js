import React, { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import QuestData from './data';
import leetcodeImage from './images/leetcode.png';
import gfgImage from './images/gfg.png';
import leetcodeDarkImage from './images/leetcode_dark.png';
import gfgDarkImage from './images/gfg_dark.png';
import './style/quest.css';
import { saveProgress, loadProgress } from './progress';
import ConfettiAnimation from './cele';
import SearchBar from './searchbar';

const Quest = ({ darkMode, setDarkMode }) => {
  const { category } = useParams();
  const navigate = useNavigate();

  const [filteredData, setFilteredData] = useState([]);
  const [progressPercentage, setProgressPercentage] = useState(0);
  const [showCelebration, setShowCelebration] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const [sortByStatus, setSortByStatus] = useState('');
  const [isDarkMode, setIsDarkMode] = useState(false);

  // Define image sources
  const imageMap = {
    leetcode: darkMode ? leetcodeDarkImage : leetcodeImage,
    gfg: darkMode ? gfgDarkImage : gfgImage
  };
  

  // Apply dark mode styles to images
  const imageStyle = {
    filter: darkMode ? 'invert(1)' : 'none', // Invert colors in dark mode
    width: '50px' // Adjust image width
  };

  const calculateProgress = () => {
    const topicData = QuestData.find(cat => cat.name === category);
    const totalQuestions = topicData ? topicData.questions.length : 0;
    const solvedQuestions = topicData ? topicData.questions.filter(question => question.solved).length : 0;
    const percentage = totalQuestions === 0 ? 0 : (solvedQuestions / totalQuestions) * 100;
    setProgressPercentage(percentage);
    saveProgress(category, percentage);
  };
  const markAsComplete = (categoryIndex, questionIndex) => {
    const updatedData = [...filteredData];
    updatedData[categoryIndex].questions[questionIndex].solved = true;
    setFilteredData(updatedData);
  };

  useEffect(() => {
    setIsDarkMode(darkMode);
  }, [darkMode]);

  useEffect(() => {
    const savedData = loadProgress(category);
    setProgressPercentage(savedData);
  }, [category]);

  useEffect(() => {
    const initialFilteredData = QuestData.filter(cat => cat.name === category);
    setFilteredData(initialFilteredData);
  }, [category]);

  useEffect(() => {
    calculateProgress();
  }, [filteredData]);

  const getPlatformFromLink = (link) => {
    if (link.includes("leetcode")) {
      return "leetcode";
    } else if (link.includes("geeksforgeeks")) {
      return "gfg";
    }
  };

  // Function to handle random selection
  const handleRandom = () => {
    const unsolvedQuestions = filteredData.flatMap(cat => cat.questions.filter(question => !question.solved));
    const randomQuestion = unsolvedQuestions[Math.floor(Math.random() * unsolvedQuestions.length)];
    if (randomQuestion) {
      // Delay the opening of the random question link by 1 second
      setTimeout(() => {
        // Construct the URL for the random question with target="_blank" attribute
        const randomQuestionUrl = randomQuestion.qlink;
        window.open(randomQuestionUrl, '_blank');
      }, 1000); // 1000 milliseconds = 1 second
    }
  };

  const handleSearch = (e) => {
    const query = e.target.value.toLowerCase();
    setSearchQuery(query);
    if (query === '') {
      setFilteredData(QuestData.filter(cat => cat.name === category));
    } else {
      const filteredQuestions = QuestData.map(cat => ({
        ...cat,
        questions: cat.questions.filter(question => question.qname.toLowerCase().includes(query))
      })).filter(cat => cat.questions.length > 0 && cat.name === category);

      if (filteredQuestions.length === 0) {
        setFilteredData(QuestData.filter(cat => cat.name === category));
      } else {
        setFilteredData(filteredQuestions);
      }
    }
  };

  const handleSortByStatus = () => {
    // Toggle sorting order between
    if (sortByStatus === 'unsolved') {
      setFilteredData(prevData => prevData.map(cat => ({
        ...cat,
        questions: cat.questions.sort((a, b) => (a.solved ? 1 : -1)) // Sort by solved status
      })));
      setSortByStatus('solved');
    } else {
      setFilteredData(prevData => prevData.map(cat => ({
        ...cat,
        questions: cat.questions.sort((a, b) => (a.solved ? -1 : 1)) // Sort by solved status
      })));
      setSortByStatus('unsolved');
    }
  };

  return (
    <div className={`container ${darkMode ? 'dark-mode' : ''}`}>
      <div className="div1">
        <div className="div2">
          <h1>{category} Practice Questions</h1>
        </div>
        <div className="div3">
          <nav>
            <Link to="/">Topics</Link> / {category}
          </nav>
        </div>
        <div className="div4">
          <div className="progress-bar">
            <div className="progress" style={{ width: `${progressPercentage}%`, backgroundColor: '#3653f8' }}></div>
          </div>
          <div className="percentage">{Math.round(progressPercentage)}% Completed</div>
        </div>
        <div className="div5 table-container">
          <SearchBar value={searchQuery} onChange={handleSearch} />
          <button className='random' onClick={handleRandom}>Random</button>
          {/* <button className='sort-status' onClick={handleSortByStatus}>Sort by Status</button> */}
          {filteredData.length === 0 ? (
            <h2>All Questions</h2>
          ) : (
            <table>
              <thead>
                <tr>
                  <th>Question</th>
                  <th className='statusss' onClick={handleSortByStatus}>Status</th>
                  <th>Platform</th>
                </tr>
              </thead>
              <tbody>
                {filteredData.map((cat, categoryIndex) => (
                  <React.Fragment key={categoryIndex}>
                    {cat.questions.map((question, questionIndex) => (
                      <tr key={questionIndex}>
                        <td className="question-cell">
                          <a href={question.qlink} target='_blank' rel="noreferrer">{question.qname}</a>
                        </td>
                        <td>
                          <button
                            onClick={() => markAsComplete(categoryIndex, questionIndex)}
                            className={question.solved ? 'complete-button green' : 'completed-button'}>
                            {question.solved ? <span className="box">Solved</span> : <span className="box-complete">Complete</span>}
                          </button>
                        </td>
                        <td>
                          <img src={imageMap[getPlatformFromLink(question.qlink)]} alt={getPlatformFromLink(question.qlink)} style={{ width: '50px' }}/>
                        </td>
                      </tr>
                    ))}
                  </React.Fragment>
                ))}
              </tbody>
            </table>
          )}
        </div>
        {showCelebration && <ConfettiAnimation />}
      </div>
    </div>
  );
};

export default Quest;

