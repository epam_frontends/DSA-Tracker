import React, { useState, useEffect } from 'react';
import './style/topic-cards.css';
import { Link } from 'react-router-dom';
import QuestData from './data';
import { loadProgress, saveProgress } from './progress';
import Navigation from './nav';
import { ThemeProvider, Button } from '@mui/material'; // Import ThemeProvider and Button from Material-UI
import { lightTheme, darkTheme } from './theme'; // Import your theme configurations

const Topic = ({ darkMode, setDarkMode }) => {
  const [startedTopics, setStartedTopics] = useState({});
  const [totalProgress, setTotalProgress] = useState(0);
  const [isDarkMode, setIsDarkMode] = useState(false); // State for dark mode toggle

  useEffect(() => {
    setIsDarkMode(darkMode);
  }, [darkMode]);

  useEffect(() => {
    // Load started topics from local storage
    const storedTopics = {};
    let totalCompletedQuestions = 0;
    let totalQuestions = 0;

    QuestData.forEach((topic) => {
      const isStarted = loadProgress(topic.name) !== null;
      storedTopics[topic.name] = isStarted;
      if (isStarted) {
        const progress = loadProgress(topic.name);
        totalCompletedQuestions += (progress / 100) * topic.questions.length;
        totalQuestions += topic.questions.length;
      }
    });

    const overallProgress = totalQuestions === 0 ? 0 : (totalCompletedQuestions / totalQuestions) * 100;
    setTotalProgress(overallProgress);
    setStartedTopics(storedTopics);
  }, []);

  const handleStartClick = (topicName) => {
    // Set the started flag for the topic
    setStartedTopics((prevTopics) => ({
      ...prevTopics,
      [topicName]: true,
    }));
    // Save the started state to local storage with progress 0
    saveProgress(topicName, 0);
  };

  const handleClearLocalStorage = () => {
    // Clear all progress from local storage
    QuestData.forEach((topic) => {
      localStorage.removeItem(`questData-${topic.name}`);
    });
    // Update state to reflect cleared local storage
    setStartedTopics({});
    setTotalProgress(0);
  };

  const toggleDarkMode = () => {
    setDarkMode(!darkMode); // Toggle dark mode state in App.js
  };

  return (
    <ThemeProvider theme={isDarkMode ? darkTheme : lightTheme}>
      <div className={`page-container ${isDarkMode ? 'theme-dark' : ''}`}>
        {/* <Navigation /> */}
        <div className='cards'>
          <div className='progress-info'>Total Progress: {Math.round(totalProgress)}% Completed</div>
          <div className='progress-bar'>
            <div className='progress' style={{ width: `${totalProgress}%` }}></div>
          </div>
          {QuestData.map((topic, index) => (
            <div className='topic-cards' key={index}>
              <div className='topic-name'>{topic.name}</div>
              <div className='topic-question-count'>Total Number of Questions: {topic.questions.length}</div>
              {startedTopics[topic.name] ? (
                <>
                  {loadProgress(topic.name) > 0 && (
                    <div className='progress-info'>
                      Progress: {Math.round(loadProgress(topic.name))}% Completed
                    </div>
                  )}
                  <Link to={`/questions/${topic.name}`} className='practice'>Solve Now</Link>
                </>
              ) : (
                <button onClick={() => handleStartClick(topic.name)} className='practice'>Start Now</button>
              )}
            </div>
          ))}
          <button onClick={handleClearLocalStorage}>Clear Local Storage</button>
          {/* <Button onClick={toggleDarkMode}>
            {isDarkMode ? 'Switch to Light Mode' : 'Switch to Dark Mode'}
          </Button> */}
        </div>
      </div>
    </ThemeProvider>
  );
};

export default Topic;
