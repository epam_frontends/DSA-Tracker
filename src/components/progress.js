// progress.js
const getLocalStorageKey = (category) => {
    return `questData-${category}`;
};
  
export const saveProgress = (category, progress) => {
    const localStorageKey = getLocalStorageKey(category);
    localStorage.setItem(localStorageKey, JSON.stringify({ progress }));
};
  
export const loadProgress = (category) => {
    const localStorageKey = getLocalStorageKey(category);
    const savedData = localStorage.getItem(localStorageKey);
    return savedData ? JSON.parse(savedData).progress : 0;
};

