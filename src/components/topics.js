import React from 'react';

const topics = ['Array', 'List', 'Queue'];

const Topic = () => {
  return (
    <div className='container-topic'>
        <div className='sidebar'>
        <ul className='topic-list'>
            {topics.map((topic, index) => (
            <li key={index}>{topic}<hr/></li>
            ))}
        </ul>
        </div>
    </div>
  );
};

export default Topic;
