const QuestData = [
    {
      name: "Strings",
      questions: [
        {
          qname: "Longest Substring Without Repeating Characters",
          qlink: "https://leetcode.com/problems/longest-substring-without-repeating-characters",
        },
        {
          qname: "Reverse words in a given string",
          qlink: "https://www.geeksforgeeks.org/problems/reverse-words-in-a-given-string5459/1",
        },
        {
          qname: "Longest Common Prefix",
          qlink: "https://www.geeksforgeeks.org/problems/longest-common-prefix-in-an-array5129/1",
        },
        {
          qname: "Roman Number to Integer",
          qlink: "https://www.geeksforgeeks.org/problems/roman-number-to-integer3201/1",
        },
        {
          qname: "Integer to Roman",
          qlink: "https://www.geeksforgeeks.org/problems/convert-to-roman-no/1",
        },
        {
          qname: "Closest Strings",
          qlink: "https://www.geeksforgeeks.org/problems/closest-strings0611/1",
        },
        {
          qname: "Divisible by 7",
          qlink: "https://www.geeksforgeeks.org/problems/divisible-by-73224/1",
        },
        {
          qname: "Encrypt the String – II",
          qlink: "https://www.geeksforgeeks.org/problems/encrypt-the-string-21117/1",
        },
        {
          qname: "Equal point in a string of brackets",
          qlink: "https://www.geeksforgeeks.org/problems/find-equal-point-in-string-of-brackets2542/1",
        },
        {
          qname: "Isomorphic Strings",
          qlink: "https://www.geeksforgeeks.org/problems/isomorphic-strings-1587115620/1",
        },
        {
          qname: "Check if two strings are k-anagrams or not",
          qlink: "https://www.geeksforgeeks.org/problems/check-if-two-strings-are-k-anagrams-or-not/1",
        },
        {
          qname: "Panagram Checking",
          qlink: "https://www.geeksforgeeks.org/problems/pangram-checking-1587115620/1",
        },
        {
          qname: "Minimum Deletions",
          qlink: "https://www.geeksforgeeks.org/problems/minimum-deletitions1648/1",
        },
        {
          qname: "Number of Distinct Subsequences",
          qlink: "https://www.geeksforgeeks.org/problems/number-of-distinct-subsequences0909/1",
        },
        {
          qname: "Check if string is rotated by two places",
          qlink: "https://www.geeksforgeeks.org/problems/number-of-distinct-subsequences0909/1https://www.geeksforgeeks.org/check-string-can-obtained-rotating-another-string-2-places/",
        },
        
      ]
    },
    {
      name: "Arrays",
      questions: [
        {
          qname: "Modify the Matrix",
          qlink: "https://leetcode.com/problems/modify-the-matrix",
        },
        {
            qname: "Find Indexes of a subarray with given sum",
            qlink: "https://www.geeksforgeeks.org/problems/subarray-with-given-sum-1587115621/1?page=1&sortBy=submissions",
        },
        {
            qname: "Missing number in array",
            qlink: "https://www.geeksforgeeks.org/problems/missing-number-in-array1416/1?page=1&category=Arrays&sortBy=submissions",
        },
        {
            qname: "Kadane's Algorithm",
            qlink: "https://www.geeksforgeeks.org/problems/kadanes-algorithm-1587115620/1?page=1&category=Arrays&sortBy=submissions",
        },
        {
            qname: "Find duplicates in an array",
            qlink: "https://www.geeksforgeeks.org/problems/find-duplicates-in-an-array/1?page=1&category=Arrays&sortBy=submissions",
        },
        {
            qname: "Leaders in an array",
            qlink: "https://www.geeksforgeeks.org/problems/leaders-in-an-array-1587115620/1?page=1&category=Arrays&sortBy=submissions",
              
        },
        {
            qname: "Majority Element",
            qlink: "https://www.geeksforgeeks.org/problems/majority-element-1587115620/1?page=1&category=Arrays&sortBy=submissions",
              
        },
        {
            qname: "Minimize the Heights II",
            qlink: "https://www.geeksforgeeks.org/problems/minimize-the-heights3351/1?page=1&category=Arrays&sortBy=submissions",    
        },
        {
            qname: "Find a peak element which is not smaller than its neighbors",
            qlink: "https://www.geeksforgeeks.org/problems/peak-element/1",
        },
        {
            qname: "Find the minimum and maximum element in an array",
            qlink: "https://www.geeksforgeeks.org/problems/find-minimum-and-maximum-element-in-an-array4428/1",    
        },
        {
            qname: "Write a program to reverse the array	",
            qlink: "https://www.geeksforgeeks.org/problems/reverse-a-string/1",
              
        },
        {
          qname: "Write a program to sort the given array",
          qlink: "https://www.geeksforgeeks.org/problems/sort-the-array0055/1",
            
      },
      {
        qname: "Find the Kth largest and Kth smallest number in an array",
        qlink: "https://www.geeksforgeeks.org/problems/kth-smallest-element5635/1",
          
       },
        {
          qname: "Find the occurrence of an integer in the array",
          qlink: "https://www.geeksforgeeks.org/problems/find-the-frequency/1",
            
      },
          {
          qname: "Sort the array of 0s, 1s, and 2s",
          qlink: "https://www.geeksforgeeks.org/problems/sort-an-array-of-0s-1s-and-2s4231/1",
            
      },
      {
        qname: "Move all the negative elements to one side of the array",
        qlink: "https://www.geeksforgeeks.org/problems/move-all-negative-elements-to-end1813/1",
        },
        {
          qname: "Find the Union and Intersection of the two sorted arrays",
          qlink: "https://www.geeksforgeeks.org/problems/union-of-two-arrays3538/1",
            
      },
      {
        qname: "Write a program to cyclically rotate an array by one",
        qlink: "https://www.geeksforgeeks.org/problems/cyclically-rotate-an-array-by-one2614/1",
          
    },
    {
      qname: "Count Pairs with the given sum",
      qlink: "https://www.geeksforgeeks.org/problems/count-pairs-with-given-sum5022/1",
        
  },
  {
    qname: "Find duplicates in an array",
    qlink: "https://www.geeksforgeeks.org/problems/find-duplicates-in-an-array/1",
      
},
        ]
    },
    {
        name: "LinkedList",
        questions: [
          {
            qname: "Add Two Numbers",
            qlink: "https://leetcode.com/problems/add-two-numbers/description/",
          },
          {
            qname: "Remove Nth Node From End of List",
            qlink: "https://leetcode.com/problems/remove-nth-node-from-end-of-list/description/",
          },
          {
            qname: "Merge Two Sorted Lists",
            qlink: "https://leetcode.com/problems/merge-two-sorted-lists/",
          },
          {
            qname: "Merge k Sorted Lists",
            qlink: "https://leetcode.com/problems/merge-k-sorted-lists/description/",
          },
          {
            qname: "Swap Nodes in Pairs",
            qlink: "https://leetcode.com/problems/swap-nodes-in-pairs/description/",
          },
          {
            qname: "Reverse Nodes in k-Group",
            qlink: "https://leetcode.com/problems/reverse-nodes-in-k-group/description/",
          },
          {
            qname: "Rotate List",
            qlink: "https://leetcode.com/problems/rotate-list/description/",
          },
          {
            qname: "Remove Duplicates from Sorted List II",
            qlink: "https://leetcode.com/problems/remove-duplicates-from-sorted-list-ii/description/",
          },
          {
            qname: "Remove Duplicates from Sorted List",
            qlink: "https://leetcode.com/problems/remove-duplicates-from-sorted-list/",
          },
          {
            qname: "Partition List",
            qlink: "https://leetcode.com/problems/partition-list/description/",
          },
          {
            qname: "Reverse Linked List II",
            qlink: "https://leetcode.com/problems/reverse-linked-list-ii/description/",
          },
          {
            qname: "Convert Sorted List to Binary Search Tree",
            qlink: "https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/",
          },
          {
            qname: "Flatten Binary Tree to Linked List",
            qlink: "https://leetcode.com/problems/flatten-binary-tree-to-linked-list/",
          },
          {
            qname: "Populating Next Right Pointers in Each Node",
            qlink: "https://leetcode.com/problems/populating-next-right-pointers-in-each-node/",
          },
          {
            qname: "Populating Next Right Pointers in Each Node II",
            qlink: "https://leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/description/",
          },
          {
            qname: "Copy List with Random Pointer",
            qlink: "https://leetcode.com/problems/copy-list-with-random-pointer/description/",
          },
          {
            qname: "Linked List Cycle",
            qlink: "https://leetcode.com/problems/linked-list-cycle/description/",
          },
          {
            qname: "Linked List Cycle II",
            qlink: "https://leetcode.com/problems/linked-list-cycle-ii/description/",
          },
          {
            qname: "Reorder List",
            qlink: "https://leetcode.com/problems/reorder-list/description/",
          },
          {
            qname: "LRU Cache",
            qlink: "https://leetcode.com/problems/lru-cache/description/",
          },

          
        ]
    },{
        name: "Searching",
        questions: [
          {
            qname: "Median of Two Sorted Arrays",
            qlink: "https://leetcode.com/problems/median-of-two-sorted-arrays/description/",
          },
          {
            qname: "Search in Rotated Sorted Array",
            qlink: "https://leetcode.com/problems/search-in-rotated-sorted-array/",
          },
          {
            qname: "Find First and Last Position of Element in Sorted Array",
            qlink: "https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/",
          },
          {
            qname: "Search Insert Position",
            qlink: "https://leetcode.com/problems/search-insert-position/",
          },
          {
            qname: "Sqrt(x)",
            qlink: "https://leetcode.com/problems/sqrtx/",
          },
          {
            qname: "Search a 2D Matrix",
            qlink: "https://leetcode.com/problems/search-a-2d-matrix/",
          },
          {
            qname: "Search in Rotated Sorted Array II",
            qlink: "https://leetcode.com/problems/search-in-rotated-sorted-array-ii/",
          },
          {
            qname: "Find Minimum in Rotated Sorted Array",
            qlink: "https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/",
          },
          {
            qname: "Find Minimum in Rotated Sorted Array II",
            qlink: "https://leetcode.com/problems/find-minimum-in-rotated-sorted-array-ii/",
          },
          {
            qname: "Find Peak Element",
            qlink: "https://leetcode.com/problems/find-peak-element/",
          },
          {
            qname: "Two Sum II - Input Array Is Sorted",
            qlink: "https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/",
          },
          {
            qname: "Minimum Size Subarray Sum",
            qlink: "https://leetcode.com/problems/minimum-size-subarray-sum/description/",
          },
          {
            qname: "Count Complete Tree Nodes",
            qlink: "https://leetcode.com/problems/count-complete-tree-nodes/",
          },
          {
            qname: " Search a 2D Matrix II",
            qlink: "https://leetcode.com/problems/search-a-2d-matrix-ii/",
          },
          {
            qname: "Missing Number",
            qlink: "https://leetcode.com/problems/missing-number/",
          },
          {
            qname: "First Bad Version",
            qlink: "https://leetcode.com/problems/first-bad-version/",
          },
          {
            qname: "Find the Duplicate Number",
            qlink: "https://leetcode.com/problems/find-the-duplicate-number/",
          },
          {
            qname: "Longest Increasing Subsequence",
            qlink: "https://leetcode.com/problems/longest-increasing-subsequence/description/",
          },
          {
            qname: "Intersection of Two Arrays",
            qlink: "https://leetcode.com/problems/intersection-of-two-arrays/",
          },
          {
            qname: "Intersection of Two Arrays II",
            qlink: "https://leetcode.com/problems/intersection-of-two-arrays-ii/description/",
          },
          {
            qname: "Valid Perfect Square",
            qlink: "https://leetcode.com/problems/valid-perfect-square/",
          },
          {
            qname: "Guess Number Higher or Lower",
            qlink: "https://leetcode.com/problems/guess-number-higher-or-lower/",
          },
          {
            qname: "Arranging Coins",
            qlink: "https://leetcode.com/problems/arranging-coins/",
          },
          
        ]
    },
    // {
    //     name: "Two Pointers",
    //     questions: [
    //       {
    //         qname: "",
    //         qlink: "",
    //       },
          
    //     ]
    // },
    {
        name: "Stacks & Queues",
        questions: [
          {
            qname: "Parenthesis Checker",
            qlink: "https://www.geeksforgeeks.org/check-for-balanced-parentheses-in-an-expression/",
          },
          {
            qname: "Reverse a String using Stack",
            qlink: "https://www.geeksforgeeks.org/stack-set-3-reverse-string-using-stack/",
          },
          {
            qname: "Why do we need Prefix and Postfix notations?",
            qlink: "https://www.geeksforgeeks.org/postfix-prefix-conversion/",
          },
          {
            qname: "Reverse an array using Stack",
            qlink: "https://www.geeksforgeeks.org/reverse-an-array-using-stack/",
          },
          {
            qname: "How would I modify my code to delete chars from the beginning?",
            qlink: "https://www.geeksforgeeks.org/remove-characters-from-the-first-string-which-are-present-in-the-second-string/",
          },
          {
            qname: "Reverse a Stack using queue",
            qlink: "https://www.geeksforgeeks.org/reverse-a-stack-using-queue/",
          },
          {
            qname: "Delete Middle element from stack",
            qlink: "https://www.geeksforgeeks.org/delete-middle-element-stack/",
          },
          {
            qname: "Reverse First k Elements of Queue",
            qlink: "https://www.geeksforgeeks.org/reversing-first-k-elements-queue/",
          },
          {
            qname: "Implement a Queue using an Array",
            qlink: "https://www.geeksforgeeks.org/array-implementation-of-queue-simple/",
          },
          {
            qname: "Print all elements of a queue in a new line",
            qlink: "https://www.geeksforgeeks.org/print-level-order-traversal-line-line/",
          },
          {
            qname: "Level with maximum number of nodes",
            qlink: "https://www.geeksforgeeks.org/level-maximum-number-nodes/",
          },
          {
            qname: "Breadth First Search or BFS for a Graph",
            qlink: "https://www.geeksforgeeks.org/breadth-first-search-or-bfs-for-a-graph/",
          },
          {
            qname: "Find Minimum Depth of a Binary Tree",
            qlink: "https://www.geeksforgeeks.org/find-minimum-depth-of-a-binary-tree/",
          },
          {
            qname: "Implement a Deque",
            qlink: "https://www.geeksforgeeks.org/implementation-deque-using-circular-array/ ",
          },
          {
            qname: "Implement a Circular Queue",
            qlink: "https://www.geeksforgeeks.org/introduction-to-circular-queue/",
          },
          {
            qname: "Check if a queue can be sorted into another queue using a stack",
            qlink: "https://www.geeksforgeeks.org/check-queue-can-sorted-another-queue-using-stack/",
          },
          {
            qname: "Implement Stack using Queues",
            qlink: "https://www.geeksforgeeks.org/implement-stack-using-queue/",
          },
        
          
        ]
    },
    //{
    //     name: "Stacks & Queues",
    //     questions: [
    //       {
    //         qname: "",
    //         qlink: "",
    //       },
          
    //     ]
    // },
    {
        name: "Hashing",
        questions: [
          {
            qname: "Find whether an array is subset of another array",
            qlink: "https://www.geeksforgeeks.org/find-whether-an-array-is-subset-of-another-array-set-1/",
          },
          {
            qname: "Union and Intersection of two Linked Lists",
            qlink: "https://www.geeksforgeeks.org/union-and-intersection-of-two-linked-lists/",
          },
          {
            qname: "Find a pair with given sum",
            qlink: "https://www.geeksforgeeks.org/problems/key-pair5616/1?itm_source=geeksforgeeks&itm_medium=article&itm_campaign=bottom_sticky_on_article",
          },
          {
            qname: "Find Itinerary from a given list of tickets",
            qlink: "https://www.geeksforgeeks.org/find-itinerary-from-a-given-list-of-tickets/",
          },
          {
            qname: "Find four elements a, b, c and d in an array such that a+b = c+d",
            qlink: "https://www.geeksforgeeks.org/problems/sum-equals-to-sum4006/1?itm_source=geeksforgeeks&itm_medium=article&itm_campaign=bottom_sticky_on_article",
          },
          {
            qname: "Find the largest subarray with 0 sum",
            qlink: "https://www.geeksforgeeks.org/find-the-largest-subarray-with-0-sum/",
          },
          {
            qname: "Count distinct elements in every window of size k",
            qlink: "https://www.geeksforgeeks.org/count-distinct-elements-in-every-window-of-size-k/",
          },
          {
            qname: "Find smallest range containing elements from k lists",
            qlink: "https://www.geeksforgeeks.org/problems/find-smallest-range-containing-elements-from-k-lists/1?itm_source=geeksforgeeks&itm_medium=article&itm_campaign=bottom_sticky_on_article",
          },
          {
            qname: "Palindrome Substring Queries",
            qlink: "https://www.geeksforgeeks.org/palindrome-substring-queries/",
          },
          {
            qname: "Largest subarray with equal number of 0s and 1s",
            qlink: "https://www.geeksforgeeks.org/problems/largest-subarray-of-0s-and-1s/1?itm_source=geeksforgeeks&itm_medium=article&itm_campaign=bottom_sticky_on_article",
          },
        
          
        ]
    },
    {
        name: "BackTracking",
        questions: [
          {
            qname: "Letter Combinations of a Phone Number",
            qlink: "https://leetcode.com/problems/letter-combinations-of-a-phone-number/description/",
          },
          {
            qname: "Generate Parentheses",
            qlink: "https://leetcode.com/problems/generate-parentheses/description/",
          },
          {
            qname: "Sudoku Solver",
            qlink: "https://leetcode.com/problems/sudoku-solver/description/",
          },
          {
            qname: "Combination Sum",
            qlink: "https://leetcode.com/problems/combination-sum/description/",
          },
          {
            qname: "Permutations",
            qlink: "https://leetcode.com/problems/permutations/",
          },
          {
            qname: "N-Queens",
            qlink: "https://leetcode.com/problems/n-queens/",
          },
          {
            qname: "Subsets",
            qlink: "https://leetcode.com/problems/subsets/",
          },
          {
            qname: "24 Game",
            qlink: "https://leetcode.com/problems/24-game/",
          },
          {
            qname: "Stickers to Spell Word",
            qlink: "https://leetcode.com/problems/stickers-to-spell-word/",
          },
          
          
        ]
    },
    {
        name: "Binary Trees",
        questions: [
          {
            qname: "Binary Tree Inorder Traversal",
            qlink: "https://leetcode.com/problems/binary-tree-inorder-traversal/",
          },
          {
            qname: "Unique Binary Search Trees II",
            qlink: "https://leetcode.com/problems/unique-binary-search-trees-ii/",
          },
          {
            qname: "Unique Binary Search Trees",
            qlink: "https://leetcode.com/problems/unique-binary-search-trees/ ",
          },
          {
            qname: "Validate Binary Search Tree",
            qlink: "https://leetcode.com/problems/validate-binary-search-tree/",
          },
          {
            qname: "Recover Binary Search Tree",
            qlink: "https://leetcode.com/problems/recover-binary-search-tree/",
          },
          {
            qname: "Same Tree",
            qlink: "https://leetcode.com/problems/same-tree/",
          },
          {
            qname: "Symmetric Tree",
            qlink: "https://leetcode.com/problems/symmetric-tree/",
          },
          {
            qname: "Binary Tree Level Order Traversal",
            qlink: "https://leetcode.com/problems/binary-tree-level-order-traversal/",
          },
          {
            qname: "Binary Tree Zigzag Level Order Traversal",
            qlink: "https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/description/",
          },
         
          
        ]
    },
    {
        name: "Graphs",
        questions: [
          {
            qname: "Connected Components in an Undirected Graph",
            qlink: "https://www.geeksforgeeks.org/problems/number-of-provinces/1",
          },
          {
            qname: "Find the number of islands",
            qlink: "https://www.geeksforgeeks.org/problems/find-the-number-of-islands/1",
          },
          {
            qname: "Detect cycle in an undirected graph",
            qlink: "https://www.geeksforgeeks.org/problems/detect-cycle-in-an-undirected-graph/1",
          },
          {
            qname: "Hamiltonian Path",
            qlink: "https://www.geeksforgeeks.org/problems/hamiltonian-path2522/1/",
          },
          {
            qname: "Prerequisite Tasks",
            qlink: "https://www.geeksforgeeks.org/problems/prerequisite-tasks/1/",
          },
          {
            qname: "Circle of Strings",
            qlink: "https://www.geeksforgeeks.org/problems/circle-of-strings4530/1",
          },
          {
            qname: "Snake and Ladder problem",
            qlink: "https://www.geeksforgeeks.org/problems/snake-and-ladder-problem4816/1",
          },
          {
            qname: "Bipartite Graph",
            qlink: "https://www.geeksforgeeks.org/problems/bipartite-graph/1",
          },
          {
            qname: "Toplogical Sort",
            qlink: "https://www.geeksforgeeks.org/problems/topological-sort/1",
          },
          
        ]
    },
  ];

    
  export default QuestData;
  