import React, { useEffect } from 'react';
import './style/cele.css'

const ConfettiAnimation = () => {
  useEffect(() => {
    const confettiColors = ['#fce18a', '#ff726d', '#b48def', '#f4306d'];
    const confettiAnimations = ['slow', 'medium', 'fast'];

    const renderConfetti = () => {
      const containerEl = document.createElement('div');
      containerEl.classList.add('confetti-container');
      document.body.appendChild(containerEl);

      setInterval(() => {
        const confettiEl = document.createElement('div');
        const confettiSize = (Math.floor(Math.random() * 3) + 7) + 'px';
        const confettiBackground = confettiColors[Math.floor(Math.random() * confettiColors.length)];
        const confettiLeft = (Math.floor(Math.random() * window.innerWidth)) + 'px';
        const confettiAnimation = confettiAnimations[Math.floor(Math.random() * confettiAnimations.length)];

        confettiEl.classList.add('confetti', `confetti--animation-${confettiAnimation}`);
        confettiEl.style.left = confettiLeft;
        confettiEl.style.width = confettiSize;
        confettiEl.style.height = confettiSize;
        confettiEl.style.backgroundColor = confettiBackground;

        containerEl.appendChild(confettiEl);

        setTimeout(() => {
          containerEl.removeChild(confettiEl);
        }, 3000);
      }, 25);

      return () => {
        document.body.removeChild(containerEl);
      };
    };

    renderConfetti();
  }, []);

  return null;
};

export default ConfettiAnimation;