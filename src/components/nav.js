// Navigation.js
import React from 'react';
import { Link } from 'react-router-dom';
import './style/nav.css'; // Import CSS for navigation styles

const Navigation = () => {
  return (
    <nav className="navbar">
      <div className="navbar-left">
        <div className="navbar-item">
          <Link to="/">Topics</Link>
        </div>
        <div className="navbar-item">
          <Link to="/profile">Profile</Link>
        </div>
      </div>
      <div className="navbar-right">
        <input type="text" placeholder="Search..." className="search-bar" />
        <div className="navbar-item">
          <Link to="/details">Details</Link>
        </div>
        <div className="navbar-item">
          <Link to="/progress">Progress</Link>
        </div>
        <div className="navbar-item">
          <Link to="/profile">
            <img src="profile-icon.png" alt="Profile" className="profile-icon" />
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default Navigation;
