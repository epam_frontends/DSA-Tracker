import './App.css';
import React, { useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import Topic from './components/Topic-cards';
import Quest from './components/questionlist';
import Error from './components/error';
import Footer from './components/Footer';

const App = () => {
  const [darkMode, setDarkMode] = useState(false);
  return (
    <div className={`App ${darkMode ? 'dark-mode' : ''}`}>
      <Routes>
        <Route exact path="/" element={<Topic darkMode={darkMode} setDarkMode={setDarkMode} />} />
        <Route path="/questions/:category" element={<Quest darkMode={darkMode} setDarkMode={setDarkMode} />} />
        <Route path="*" element={<Error />} />
      </Routes>
      <Footer darkMode={darkMode} setDarkMode={setDarkMode} />
    </div>
  );
}

export default App;
  